import React, { Component } from "react";
import './Input.css';



export default class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: "",
    };
  }

  InputSubmit = (e) => {
    e.preventDefault();
    this.props.InputMessage(this.state.message);
    this.setState({ message: "" });
  };

  InputChange = (e) => {
    this.setState({ message: e.target.value });
  };

  render() {
    return (
      <div className="input-container">
        <form onSubmit={this.InputSubmit}>
          <input
            type="text"
            value={this.state.message}
            onChange={this.InputChange}
            placeholder="Enter your message..."
            required
            className="input-input"
          />
          <input className="input-button" type="submit" value="Send" />
        </form>
      </div>
    );
  }
}
