import React, { Component } from "react";
import './Login.css';
import { v4 as uuidv4 } from 'uuid';


export default class App extends Component {
    constructor(props) {
    super(props);

    this.state = {
        username: "",
        listAvatar: [],
        choiceAvatar: ""
    };
  };

  usernameChange = (e) => {
    this.setState({ username: e.target.value });
  }

  avatarList = (e) => {
    let random, image;
    let choice = e.target.value;

    let tempList = [];

    for(let i = 0 ; i < 8; i++) {
      random = (Math.random() * (100-1)).toFixed(0);
      image = `https://randomuser.me/api/portraits/thumb/${choice}/${random}.jpg`;

      tempList = [...tempList, image];
    }
    this.setState({ listAvatar: tempList });
  }
  avatarImg = (e) => {
    this.setState({ choiceAvatar: e.target.src });
  }
  loginSubmit = (e) => {
    e.preventDefault();
    
    if((this.state.username === "") || (this.state.choiceAvatar === "")){
      return;
    }
    this.props.Login(this.state);
  }
  
  render() {
    return (
      <div className="login-container">
          <div className="login-header">
            <h2>Login</h2>
          <div className="login-form">
          <form onSubmit={this.loginSubmit}>
            <div className="input-username">
              <p>Username:</p> <br/>
                <input
                  type="text" 
                  placeholder="Enter your username"
                  value={this.state.username} 
                  onChange={this.usernameChange} />
            </div>
            <div className="input-username">
              <p>You are:</p>
            </div>
            <div className="input-checkbox">
            <label>
              <input
                type="radio"
                name="radio-button"
                value="men"
                onClick={this.avatarList} />
            Male</label>
            <label>
              <input
                type="radio"
                name="radio-button"
                value="women"
                onClick={this.avatarList} />
              Woman</label> 
            </div>
            
           {this.state.listAvatar ?
          <div className="login-avatar-container">
             <p>Choice your avatar:</p> 
             {this.state.listAvatar.map((image, i)=>(
               <div className="login-avatar" onClick={this.avatarImg} key={uuidv4()}>
                   <img className="image" src={image} alt="avatarImage" />
                 </div> )
             )}
          </div>: null
          } 
             
             {this.state.choiceAvatar
             ? (<div className="choise-avatar">
              <p>Your avatar is:</p>
              <img className="image" key={uuidv4()} src={this.state.choiceAvatar} alt="avatarImage" /></div>) 
            : null}
              <input
              className="input-button"
              type="submit" 
              value="Login"/>
            </form>
          </div>
        </div>
      </div>
    );
  }
}



